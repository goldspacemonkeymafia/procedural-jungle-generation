namespace octet {

  class CameraHelper {
		mat4t* cameraToWorld;

		float angleX;
    float _translationFactor;
    float _rotationFactor;

    static float getDefaultTranslationFactor() {
      return 1.0f / 2.0f;
    };
    
    static float getDefaultRotationFactor() {
      return 5.0f;
    };

    mat4t& getDummyCamera() const {
      static mat4t camera;

      // Always reset dummy camera
      camera.loadIdentity();
      return camera;
    };

	public:
		CameraHelper() :
      cameraToWorld(NULL),
      angleX(0.0f),
      _translationFactor(getDefaultTranslationFactor()),
      _rotationFactor(getDefaultRotationFactor()) {
    };

		CameraHelper(camera_instance* cameraInstance) :
      cameraToWorld(&(cameraInstance->get_node()->access_nodeToParent())),
      angleX(0.0f),
      _translationFactor(getDefaultTranslationFactor()),
      _rotationFactor(getDefaultRotationFactor()) {
    };

		CameraHelper(mat4t* cameraToWorld) :
      cameraToWorld(cameraToWorld),
      angleX(0.0f),
      _translationFactor(getDefaultTranslationFactor()),
      _rotationFactor(getDefaultRotationFactor()) {
    };
      
    void setCamera(camera_instance* cameraInstance) {
      setCamera(&(cameraInstance->get_node()->access_nodeToParent()));
    };

    void setCamera(mat4t* cameraToWorld) {
      this->cameraToWorld = cameraToWorld;
    };

    float getTranslationFactor() const {
      return _translationFactor;
    };

    void setTranslationFactor(float translationFactor) {
      _translationFactor = translationFactor;
    };

    float getRotationFactor() const {
      return _rotationFactor;
    };
    
    void setRotationFactor(float rotationFactor) {
      _rotationFactor = rotationFactor;
    };

		// if I don't return reerence then I can't see anything because it is not modified that way
		mat4t& getCamera () {
      // Safe version
      //return (cameraToWorld == NULL ? getDummyCamera() : *cameraToWorld);

      return *cameraToWorld;
    };

    const mat4t& getCamera () const {
      return *cameraToWorld;
    };

		void translateLeft () { getCamera().translate(-getTranslationFactor(), 0.0f, 0.0f); }
		void translateRight () { getCamera().translate(getTranslationFactor(), 0.0f, 0.0f); }
		void translateForward () { getCamera().translate( 0.0f, 0.0f, -getTranslationFactor()); }
		void translateBack () { getCamera().translate( 0.0f, 0.0f, getTranslationFactor()); }
		void rotateLeft () { 
			getCamera().rotateX( -angleX );
			getCamera().rotateY( getRotationFactor() );
			getCamera().rotateX( angleX );
		}
		void rotateRight () { 
			getCamera().rotateX( -angleX );
			getCamera().rotateY( -getRotationFactor() );
			getCamera().rotateX( angleX );
		}
		void rotateUp () { 
			angleX += getRotationFactor();
			getCamera().rotateX( getRotationFactor() ); 
		}
		void rotateDown () { 
			angleX -= getRotationFactor();
			getCamera().rotateX( -getRotationFactor() );
		}
    void rotateDown ( float angle) { 
			angleX -= angle;
			getCamera().rotateX( -angle );
		}
		void reset () {
			reset(0.0f, 0.0f, 5.0f);
		}
		void reset(float x, float y, float z) {
			getCamera().loadIdentity();
			getCamera().translate(x, y, z);
			angleX = 0.0f;
		}
	};
  
  class CameraKeyboardHandler {
  private:
    app* _app;
    CameraHelper _camera;
    
  public:
    CameraKeyboardHandler() :
      _app(NULL) {
    };

    void attach(app* app) {
      _app = app;
    };
    
    const CameraHelper& getCameraHelper() const {
      return _camera;
    };

    CameraHelper& getCameraHelper() {
      return _camera;
    };

    void setCamera(camera_instance* cameraInstance) {
      getCameraHelper().setCamera(cameraInstance);
    };

    void setCamera(mat4t* cameraToWorld) {
      getCameraHelper().setCamera(cameraToWorld);
    };

    void update() {
      // translate camera left
		  if (_app->is_key_down('A')) {
			  getCameraHelper().translateLeft();
		  }
		  // translate camera right
		  if (_app->is_key_down('D')) {
			  getCameraHelper().translateRight();
		  }
		  // translate camera forward
		  if (_app->is_key_down('W')) {
			  getCameraHelper().translateForward();
		  }
		  // translate camra backwards
		  if (_app->is_key_down('S')) {
			  getCameraHelper().translateBack();
		  }
		 
      // pan camera up
		  if (_app->is_key_down(key_up)) {
			  getCameraHelper().rotateUp();
		  }
		  // pan camera down
		  if (_app->is_key_down(key_down)) {
			  getCameraHelper().rotateDown();
		  }
		  // pan camera left
		  if (_app->is_key_down(key_left)) {
			  getCameraHelper().rotateLeft();
		  }
		  // pan camera right
		  if (_app->is_key_down(key_right)) {
			  getCameraHelper().rotateRight();
		  }

      // reset camera
		  if (_app->is_key_down(key_space)) {
			  getCameraHelper().reset();
		  }
    };
  };

} // namespace octet