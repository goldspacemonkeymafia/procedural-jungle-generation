////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012-2014
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//
// Terrain shader

namespace octet { namespace shaders {
  class terrain_shader : public shader {
    // indices to use with glUniform*()

    // index for model space to projection space matrix
    GLuint modelToProjectionIndex_;
    GLuint modelToCamera_index;     // second matrix used for lighting maps model to camera space

    // index for texture sampler
    GLuint samplers_index;
    GLuint fog_index;
  public:
    void init() {
      // this is the vertex shader.
      // it is called for each corner of each triangle
      // it inputs pos and uv from each corner
      // it outputs gl_Position and uv_ to the rasterizer
      const char vertex_shader[] = SHADER_STR(
        varying vec2 uv_;
        varying vec4 pos_;
        varying float distanceFromCamera;

        attribute vec4 pos;
        attribute vec2 uv;

        uniform mat4 modelToProjection;
        uniform mat4 modelToCamera;

        void main() { 
          gl_Position = modelToProjection * pos; uv_ = uv; pos_ = pos;
          vec4 cs_position = modelToCamera * pos;
          distanceFromCamera = -cs_position.z;
        }
      );

      // this is the fragment shader
      // after the rasterizer breaks the triangle into fragments
      // this is called for every fragment
      // it outputs gl_FragColor, the color of the pixel and inputs uv_
      const char fragment_shader[] = SHADER_STR(
        varying vec2 uv_;
        varying vec4 pos_;
        varying float distanceFromCamera;

        uniform sampler2D samplers[3];
        uniform int fog;

        float RemapValClamped(float flInput, float flInLo, float flInHi, float flOutLo, float flOutHi)
        {
          if (flInput < flInLo)
            return flOutLo;
          if (flInput > flInHi)
            return flOutHi;
          return (((flInput-flInLo) / (flInHi-flInLo)) * (flOutHi-flOutLo)) + flOutLo;
        }

        void main() { 
          vec4 tex_color = vec4(0.0);

          if(pos_.y <= -2.0) {
            tex_color = texture2D(samplers[0], uv_*8.0);
          } else if(pos_.y > -2.0 && pos_.y <= -1.0) {
            tex_color = mix(texture2D(samplers[0], uv_*8.0), texture2D(samplers[1], uv_*8.0),(pos_.y+2.0)/1.0);
          } else if(pos_.y > -1.0 && pos_.y <= 1.0) {
            tex_color = texture2D(samplers[1], uv_*8.0);
          } else if(pos_.y > 1.0 && pos_.y <= 2.0) {
            tex_color = mix(texture2D(samplers[1], uv_*8.0), texture2D(samplers[2], uv_*8.0),(pos_.y-1.0)/1.0);
          } else if(pos_.y > 2.0 ) {
            tex_color = texture2D(samplers[2], uv_*8.0);
          } /*else if(pos_.y > 2.0 && pos_.y <= 3.0) {
            tex_color = mix(texture2D(samplers[2], uv_), texture2D(samplers[3], uv_),(pos_.y-2.0)/1.0);
          } else {
            tex_color = texture2D(samplers[3], uv_);
          }*/
          if ( fog == 0 )
            gl_FragColor = tex_color; 
          else {
            vec4 vecFogColor = vec4(0.76, 0.8, 0.85, 1.0);  
            float flFog =  RemapValClamped(abs(distanceFromCamera), 50.0, 100.0, 0.1, 0.8);
            gl_FragColor = tex_color+ vecFogColor*flFog;
          }
        }
      );
    
      // use the common shader code to compile and link the shaders
      // the result is a shader program
      shader::init(vertex_shader, fragment_shader);

      // extract the indices of the uniforms to use later
      modelToProjectionIndex_ = glGetUniformLocation(program(), "modelToProjection");
      samplers_index = glGetUniformLocation(program(), "samplers");
      fog_index = glGetUniformLocation(program(), "fog");
      modelToCamera_index = glGetUniformLocation(program(), "modelToCamera");
    }

    void render(const mat4t &modelToProjection, const mat4t &modelToCamera, const vec4 *light_uniforms, int num_light_uniforms, int num_samplers, int fog = 0) {
      // tell openGL to use the program
      shader::render();

      //printf("Terrain shader");

      // customize the program with uniforms
      // we use textures 6-8 for material properties.
      static const GLint samplers[] = { 6, 7, 8 };
      num_samplers = 3;
      glUniform1iv(samplers_index, num_samplers, samplers);
      glUniformMatrix4fv(modelToProjectionIndex_, 1, GL_FALSE, modelToProjection.get());
      glUniformMatrix4fv(modelToCamera_index, 1, GL_FALSE, modelToCamera.get());
      glUniform1i(fog_index, fog);
    }
  };
}}
