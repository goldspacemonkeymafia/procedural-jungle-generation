////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012-2014
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//
// Terrain shader

namespace octet { namespace shaders {
  class water_shader : public shader {
    // indices to use with glUniform*()

    // index for model space to projection space matrix
    GLuint modelToProjectionIndex_;
    GLuint modelToCameraIndex_;

    GLuint light_uniforms_index;    // lighting parameters for fragment shader
    GLuint num_lights_index;        // how many lights?

    GLuint time_Index;
    static float time;

    //static float A[5];
    GLuint A_index;
    ////magnitude(k)=2*pi*waveLength
    //static vec2 k[5];
    GLuint k_index;
    ////omega = g*magnitude(k)*2*pi/T; T=3
    //static float omega[5];
    GLuint omega_index;
    //static float fi[5];
    GLuint fi_index;
    
    // index for texture sampler
    GLuint samplers_index;
  public:
    void init() {
      /*A[0] = 1.0;
      A[1] = 2.0;
      A[2] = 3.0;
      A[3] = 4.0;
      A[4] = 5.0;
      
      k[0] = vec2(1.0,1.0);
      k[1] = vec2(1.0,-2.0);
      k[2] = vec2(-3.0,-1.0);
      k[3] = vec2(-1.0,-3.0);
      k[4] = vec2(-1.0,4.0);
      
      omega[0] = 644.5;
      omega[1] = 322.1;
      omega[2] = 214.8;
      omega[3] = 161.0;
      omega[4] = 128.8;

      fi[0] = 3.0;
      fi[1] = 4.0;
      fi[2] = 5.0;
      fi[3] = 6.0;
      fi[4] = 7.0;*/

      // this is the vertex shader.
      // it is called for each corner of each triangle
      // it inputs pos and uv from each corner
      // it outputs gl_Position and uv_ to the rasterizer
      const char vertex_shader[] = SHADER_STR(
        varying vec2 uv_;
        varying vec4 pos_;
        varying vec3 normal_;
        varying vec3 tangent_;
        varying vec3 bitangent_;

        attribute vec4 pos;
        attribute vec2 uv;
        attribute vec3 normal;
        attribute vec3 tangent;
        attribute vec3 bitangent;

        uniform mat4 modelToProjection;
        uniform mat4 modelToCamera;
        uniform float time;

        uniform float A[5];
        uniform float k[10];
        uniform float omega[5];
        uniform float fi[5];

        void main() { 
          vec4 tempPos = pos;

          //height of the waves
          //float A = 5.0;
          vec2 oldPos = vec2(tempPos.x/10.0, tempPos.z/30.0);
          //vec2 waveVector = vec2(1.0, 6.4);
          //float omega = 6.2831*2.0;
          float sum = 0.0;
          for (int i=0; i<5; i++){
            vec2 kVec = vec2(k[i*2],k[i*2+1]);
            sum += A[i] * cos(dot(oldPos,kVec) - omega[i] * time/50.0 + fi[i]);
          }
          tempPos.y = sum;
          gl_Position = modelToProjection * tempPos; 
          uv_ = uv; 
          pos_ = tempPos;
          normal_ = normal;
          tangent_ = (modelToCamera * vec4(tangent,0.0)).xyz;
          bitangent_ = (modelToCamera * vec4(bitangent,0.0)).xyz;
          }
      );

      // this is the fragment shader
      // after the rasterizer breaks the triangle into fragments
      // this is called for every fragment
      // it outputs gl_FragColor, the color of the pixel and inputs uv_
      const char fragment_shader[] = SHADER_STR(
        const int max_lights = 4;
        varying vec2 uv_;
        varying vec3 normal_;
        varying vec3 tangent_;
        varying vec3 bitangent_;
        varying vec4 pos_;

        uniform vec4 light_uniforms[1+max_lights*4];
        uniform int num_lights;
        uniform sampler2D samplers[6];
      
        void main() {
          vec3 X = dFdx(pos_.xyz);
          vec3 Y = dFdy(pos_.xyz);
          vec3 normal = normalize(cross(X,Y));

          float shininess = texture2D(samplers[5], uv_).x * 255.0;
          vec3 bump = normalize(vec3(texture2D(samplers[4], uv_).xy-vec2(0.5, 0.5), 1));
          vec3 nnormal = normalize(normal); //normalize(bump.x * tangent_ + bump.y * bitangent_ + bump.z * normal_);
          vec3 diffuse_light = vec3(0.3, 0.3, 0.3);
          vec3 specular_light = vec3(0, 0, 0);

          for (int i = 0; i != num_lights; ++i) {
            vec3 light_direction = light_uniforms[i * 4 + 2].xyz;
            vec3 light_color = light_uniforms[i * 4 + 3].xyz;
            vec3 half_direction = normalize(light_direction + vec3(0, 0, 1));

            float diffuse_factor = max(dot(light_direction, nnormal), 0.0);
            float specular_factor = pow(max(dot(half_direction, nnormal), 0.0), shininess) * diffuse_factor;

            diffuse_light += diffuse_factor * light_color;
            specular_light += specular_factor * light_color;
          }

          vec4 diffuse = texture2D(samplers[0], uv_);
          vec4 ambient = texture2D(samplers[1], uv_);
          vec4 emission = texture2D(samplers[2], uv_);
          vec4 specular = texture2D(samplers[3], uv_);

          vec3 ambient_light = light_uniforms[0].xyz;

          gl_FragColor.xyz = 
            ambient_light * ambient.xyz +
            diffuse_light * diffuse.xyz +
            emission.xyz +
            specular_light * specular.xyz;
          gl_FragColor.w = diffuse.w;
          //gl_FragColor = emission;
          //gl_FragColor = vec4(diffuse_light, 1);
          //gl_FragColor = vec4(num_lights, num_lights, num_lights, 1);
          // how to debug your fragment shader: set gl_FragColor to the value you want to look at!
          //gl_FragColor = vec4(1, 1, 0, 1);
          }
      );
    
      // use the common shader code to compile and link the shaders
      // the result is a shader program
      shader::init(vertex_shader, fragment_shader);

      // extract the indices of the uniforms to use later
      modelToProjectionIndex_ = glGetUniformLocation(program(), "modelToProjection");
      modelToCameraIndex_ = glGetUniformLocation(program(), "modelToCamera");
      samplers_index = glGetUniformLocation(program(), "samplers");
      time_Index = glGetUniformLocation(program(), "time");
      light_uniforms_index = glGetUniformLocation(program(), "light_uniforms");
      num_lights_index = glGetUniformLocation(program(), "num_lights");
      A_index = glGetUniformLocation(program(), "A");
      k_index = glGetUniformLocation(program(), "k");
      omega_index = glGetUniformLocation(program(), "omega");
      fi_index = glGetUniformLocation(program(), "fi");

    }

    void render(const mat4t &modelToProjection, const mat4t &modelToCamera, const vec4 *light_uniforms, int num_light_uniforms, int num_samplers, int fog = 0 ) {
      // tell openGL to use the program
      shader::render();
      time += 0.5;

      //printf("Terrain shader");

      // customize the program with uniforms
      // we use texture  for material properties.
      static const GLint samplers[] = { 0, 1, 2, 3, 4, 5 };
      num_samplers = 6;
      float A[5] = {0.4f, 0.8f, 0.7f, 0.2f, 0.3f};
      //magnitude(k)=2*pi*waveLength
      /*float k[10] = {sqrt((2.0*3.14/0.5)*(2.0*3.14/0.5)-0.5*0.5), 0.5, 
                     sqrt((2.0*3.14/0.7)*(2.0*3.14/0.7)-0.3*0.3), -0.3,
                     -sqrt((2.0*3.14/1.0)*(2.0*3.14/1.0)-0.4*0.4), 0.4,
                     -sqrt((2.0*3.14/1.5)*(2.0*3.14/1.5)-0.2*0.2), -0.2,
                     sqrt((2.0*3.14/2.0)*(2.0*3.14/2.0)-0.3*0.3), -0.3};*/
      float k[10] = {1.0f,  3.0f,
                     1.0f, -3.0f,
                    -1.0f,  2.0f,
                     1.0f,  0.0f,
                     0.0f, -1.0f};
      //omega = sqrt(g*magnitude(k))
      float omega[5] = {0.3f, 2.0f, 0.9f, 0.5f, 0.4f};
      //phase
      float fi[5] = {3.0f, 4.0f, 5.0f, 6.0f, 7.0f};

      glUniform1iv(samplers_index, num_samplers, samplers);
      glUniformMatrix4fv(modelToProjectionIndex_, 1, GL_FALSE, modelToProjection.get());
      glUniformMatrix4fv(modelToCameraIndex_, 1, GL_FALSE, modelToCamera.get());
      glUniform1f(time_Index, time);
      glUniform4fv(light_uniforms_index, num_light_uniforms, (float*)light_uniforms);
      glUniform1i(num_lights_index, num_light_uniforms);
      glUniform1fv(A_index, 5, A);
      glUniform1fv(omega_index, 5, omega);
      glUniform1fv(fi_index, 5, fi);
      glUniform1fv(k_index, 10, k);

    }
  };
  float water_shader::time=0.0;
}}
